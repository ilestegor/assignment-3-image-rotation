#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H
#pragma once

#include <stdint.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount; //number of bits per pixel
    uint32_t biCompression;
    uint32_t biSizeImage; //size of raw bitmap data with padding
    uint32_t biXPelsPerMeter;//width in pixels
    uint32_t biYPelsPerMeter;//height in pixels
    uint32_t biClrUsed;//number of colors in the palette
    uint32_t biClrImportant;
};


#endif //IMAGE_TRANSFORMER_BMP_H
