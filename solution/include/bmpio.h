#ifndef IMAGE_TRANSFORMER_BMPIO_H
#define IMAGE_TRANSFORMER_BMPIO_H
#include <image.h>
#include <stdio.h>

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_HEADER_ERROR,
    READ_ERROR,
    READ_INVALID_LINE
};

enum read_status from_bmp(FILE* in, struct image** img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, struct image* img);



#endif //IMAGE_TRANSFORMER_BMPIO_H
