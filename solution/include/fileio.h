#ifndef IMAGE_TRANSFORMER_FILEIO_H
#define IMAGE_TRANSFORMER_FILEIO_H
#include <bmpio.h>
#include <stdio.h>
enum file_open{
    OPEN_OK,
    OPEN_ERROR,
    INVALID_FILE_NAME
};
enum file_close{
    CLOSE_OK,
    CLOSE_ERROR
};
enum file_close close_file(FILE* file);
enum file_open open_file(const char* fn, FILE** file, const char* open_type);
#endif //IMAGE_TRANSFORMER_FILEIO_H
