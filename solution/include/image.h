#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#pragma once
#include "bmp.h"
#include <stdint.h>
#define GET_PADDING(row_size) ((4) - (((row_size) % (4))) % (4))
#define IMAGE_DESTROY(img) \
    free((img)->data);        \
    free(img);              \
    (img) = NULL;

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;
    struct pixel *data;
};
struct image* image_create(uint32_t width, uint32_t height);

uint32_t get_bits_row_size(struct image* img);
#endif //IMAGE_TRANSFORMER_IMAGE_H
