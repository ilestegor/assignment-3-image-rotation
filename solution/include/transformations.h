#ifndef IMAGE_TRANSFORMER_TRANSFORMATIONS_H
#define IMAGE_TRANSFORMER_TRANSFORMATIONS_H
#include "image.h"
struct image* rotate_by_90(struct image* img);

struct image* rotate_by_angle(struct image* img, uint32_t angle);

#endif //IMAGE_TRANSFORMER_TRANSFORMATIONS_H
