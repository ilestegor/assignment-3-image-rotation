#ifndef IMAGE_TRANSFORMER_VALIDATION_H
#define IMAGE_TRANSFORMER_VALIDATION_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define ARGS_OK 0
enum angle_validation{
    ANGLE_OK,
    ANGLE_NOT_FOUND
};
enum angle_validation is_valid_angle(uint32_t angle);
uint32_t validate_arg_count(uint32_t args_needed, uint32_t args_provided);
#endif //IMAGE_TRANSFORMER_VALIDATION_H
