#include "image.h"
#include <bmp.h>
#include <bmpio.h>
#include <stdio.h>
#include <stdlib.h>

#define BM_HEADER 0x4D42
#define BM_RESERVED 0
#define B_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define CLRS_USED 0
#define CLRS_IMPORTANT 0
enum read_status from_bmp(FILE *in, struct image **img) {
    //init empty struct
    struct bmp_header bmpHeader = {0};
    //reading bmp header
    size_t read = fread(&bmpHeader, sizeof(struct bmp_header), 1, in);
    if (read == 0)
        return READ_HEADER_ERROR;
    if (bmpHeader.bfType != BM_HEADER)
        return READ_INVALID_HEADER;
    //return created struct image
    *img = image_create(bmpHeader.biWidth, bmpHeader.biHeight);
    if (img ==  NULL) {
        return READ_INVALID_BITS;
    }

    if ((*img)->data != NULL) {
        if (fseek(in, bmpHeader.bOffBits, SEEK_SET) != 0) {
            return READ_INVALID_LINE;
        }
        uint32_t bits_rows_size = get_bits_row_size((*img));
        uint32_t padding = GET_PADDING(bits_rows_size);
        uint32_t rows_read = 0;
        while (rows_read < bmpHeader.biHeight) {
            if (fread((*img)->data + rows_read * (*img)->width, sizeof(struct pixel), bmpHeader.biWidth, in) != (*img)->width) {
                IMAGE_DESTROY((*img))
                return READ_ERROR;
            }
            if(fseek(in, (long)padding, SEEK_CUR)!=0){
                IMAGE_DESTROY((*img))
                return READ_INVALID_BITS;
            }
            rows_read++;
        }
        return READ_OK;
    } else
        return READ_ERROR;
}

//create new header for output image
static struct bmp_header init_header(struct image *img) {
    struct bmp_header new_header = {0};
    uint32_t row_size = get_bits_row_size(img);
    uint32_t padding = GET_PADDING(row_size);
    uint32_t file_size = (row_size + padding) * img->height + sizeof(struct bmp_header);
    uint32_t image_size = img->width * img->height * sizeof(struct pixel);

    new_header.bfType = BM_HEADER;
    new_header.biSizeImage = image_size;
    new_header.bfileSize = file_size;
    new_header.bfReserved = BM_RESERVED;
    new_header.bOffBits = sizeof(struct bmp_header);
    new_header.biSize = B_SIZE;
    new_header.biWidth = img->width;
    new_header.biHeight = img->height;
    new_header.biPlanes = PLANES;
    new_header.biBitCount = sizeof(struct pixel) * 8;
    new_header.biCompression = COMPRESSION;
    new_header.biXPelsPerMeter = X_PELS_PER_METER;
    new_header.biYPelsPerMeter = Y_PELS_PER_METER;
    new_header.biClrUsed = CLRS_USED;
    new_header.biClrImportant = CLRS_IMPORTANT;
    return new_header;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    struct bmp_header header = init_header(img);
    //write bmp header
    size_t bmp_h = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (bmp_h != 1) {
        return WRITE_ERROR;
    }

    //write pixels, calculate padding and if needed add padding
    for (uint32_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        uint32_t padding = GET_PADDING(get_bits_row_size(img));
        if (padding > 0) {
            uint32_t pad_bytes[4] = {0};
            fwrite(&pad_bytes, 1, padding, out);
        }
    }
    return WRITE_OK;
}
