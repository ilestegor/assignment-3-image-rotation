#include "transformations.h"
#include "fileio.h"
#include <stdio.h>

enum file_open open_file(const char* fn, FILE **file, const char* open_type){
    if (!fn) {
        return INVALID_FILE_NAME;
    }
    *file = fopen(fn, open_type);
    if (*file == NULL)
        return OPEN_ERROR;
    return OPEN_OK;
}
enum file_close close_file(FILE* file){
    if (fclose(file) == EOF)
        return CLOSE_ERROR;
    return CLOSE_OK;
}
