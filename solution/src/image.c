#include "image.h"
#include <stdio.h>
#include <stdlib.h>


struct image *image_create(const uint32_t width, const uint32_t height) {
    struct image *img = NULL;
    img = (struct image *) malloc(sizeof(struct image));
    if (img == NULL) {
        return NULL;
    }
    img->width = width;
    img->height = height;

    img->data = (struct pixel *) malloc(width * height * sizeof(struct pixel));
    if (img->data == NULL) {
        free(img);
        img = NULL;
        return NULL;
    }
    return img;
}

uint32_t get_bits_row_size( struct image *img) {
    return sizeof(struct pixel) * img->width;
}
