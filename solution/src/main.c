#include "fileio.h"
#include "transformations.h"
#include "validation.h"
#include <stdlib.h>
#include <string.h>

#define SOURCE_IMAGE_FN_INDEX 1
#define TARGET_FN_INDEX 2
#define ANGLE_INDEX 3
#define DEFINE_PRINT(stream) \
        void print_##stream(const char* message){ \
            fprintf(stream, "%s\n", message);                        \
        }
//defining printer functions
DEFINE_PRINT(stderr)
DEFINE_PRINT(stdout)
#undef DEFINE_PRINT
//argv[0] - executable file
//argv[1] - source image
//argv[2] - transformed image
//argv[3] - angle

int main(int argc, char **argv) {
    //validate arg count
    uint32_t validated_arg_count = validate_arg_count((uint32_t)4, (uint32_t)argc);
    if (validated_arg_count != 0) {
        print_stdout("fds");
        print_stderr(
                "Invalid number of arguments. Format should be: <exec> <source-image> <transformed-image> <angle>");
        return EXIT_FAILURE;
    }
//    if all args are in place put them in variables
    const char *source_image_fn = argv[SOURCE_IMAGE_FN_INDEX];
    const char* transformed_image_fn = argv[TARGET_FN_INDEX];
    const char *angle_str = argv[ANGLE_INDEX];
    char *remaining;
    uint32_t angle = strtol(angle_str, &remaining, 10);

//    //validate angle
    uint32_t validated_angle = is_valid_angle(angle);
    if (validated_angle != ANGLE_OK || strlen(remaining) != 0) {
        print_stderr("Angle is not valid. Angle must be int value from this range: -90, 90, -180, 180, -270, 270");
        return EXIT_FAILURE;
    }
    //open file
    FILE *source_file;
    uint32_t opened_file = open_file(source_image_fn, &source_file, "rb");
    if (opened_file != OPEN_OK) {
        print_stderr("File could not be opened");
        return EXIT_FAILURE;
    }

    //if file is opened successfully => create struct and read image
    struct image *source_image = {0};
    uint32_t read_through_image = from_bmp(source_file, &source_image);
    if (read_through_image != READ_OK) {
        print_stderr("Something went wrong while reading image from file. EXIT");
        return EXIT_FAILURE;
    }
    //close source file cause we dont need it anymore
    if (close_file(source_file) != CLOSE_OK) {
        print_stderr("Source file can`t be closed");
            return EXIT_FAILURE;
    }

    //rotation part - start

    //create new image
    struct image *rotated_image = {0};
    rotated_image = rotate_by_angle(source_image, angle);
    if (!rotated_image){
        print_stderr("New image was created unsuccessfully");
        return EXIT_FAILURE;
    }

    //rotation part - end

    //if image was successfully rotated -> write it in a target file
    FILE* target_file;
    uint32_t opened_target_file = open_file(transformed_image_fn, &target_file, "wb+");
    if (opened_target_file != OPEN_OK){
        print_stderr("Target file could not be opened");
        return EXIT_FAILURE;
    }

    //if file was opened successfully -> write image
    uint32_t image_written_status = to_bmp(target_file, rotated_image);
    if (image_written_status != WRITE_OK){
        print_stderr("Something went wrong while writing image to target file. EXIT");
        return EXIT_FAILURE;
    }
    IMAGE_DESTROY(rotated_image)
    //close target file
    uint32_t close_target_file_status = close_file(target_file);
    if (close_target_file_status != CLOSE_OK){
        print_stderr("Target file could not be close");
        return EXIT_FAILURE;
    }
    return 0;
}
