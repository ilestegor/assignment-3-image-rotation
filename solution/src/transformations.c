#include "transformations.h"
#include <stdio.h>
#include <stdlib.h>
struct image* rotate_by_90(struct image* img){
    struct image* rotated = image_create(img->height, img->width);
    if (!rotated)
        return NULL;
    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            //convert 2d index to 1d index
            uint32_t old_index = (i * img->width + j);
            //find new rotated index
            uint32_t new_index = ((img->width - j - 1) * img->height + i);
            rotated->data[new_index] = img->data[old_index];

        }
    }
    return rotated;
}

static uint32_t get_rotations_by_90(uint32_t angle){
    return ((angle + 360) % 360) / 90;
}
struct image* rotate_by_angle(struct image* img, const uint32_t angle){
    uint32_t rotations = get_rotations_by_90(angle);
    struct image* rotated = {0};
    if (rotations == 0)
        return img;
    if (rotations < 0)
        return NULL;
    for (size_t i = 0; i < rotations; ++i) {
        rotated = rotate_by_90(img);
        //free prev image that was rotated on last loop
        IMAGE_DESTROY(img)
        img = rotated;
    }
    return rotated;
}
