#include "validation.h"
#define acceptable_angles (uint32_t[]){0, -90, 90, 180, -180, 270, -270}

enum angle_validation is_valid_angle(uint32_t angle){
    for(size_t i=0 ; i<sizeof(acceptable_angles)/sizeof(*acceptable_angles) ; i++){
        if(acceptable_angles[i] == angle){
            return ANGLE_OK;
        }
    } return ANGLE_NOT_FOUND;
}

uint32_t validate_arg_count(uint32_t args_needed, uint32_t args_provided){
    if (args_provided != args_needed)
        return EXIT_FAILURE;
    return ARGS_OK;
}
